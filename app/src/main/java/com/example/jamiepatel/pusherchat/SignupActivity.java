package com.example.jamiepatel.pusherchat;

import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;


public class SignupActivity extends ActionBarActivity  {

    final String MESSAGES_ENDPOINT = "http://46.101.215.26:3000";

    private static final int SELECT_PICTURE = 0;

    String photoPath;

    EditText usernameInput;
    EditText passwordInput;
    EditText nameInput;
    EditText ageInput;
    Button signupButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        usernameInput = (EditText) findViewById(R.id.username_input);
        passwordInput = (EditText) findViewById(R.id.password_input);
        nameInput = (EditText) findViewById(R.id.name_input);
        ageInput = (EditText) findViewById(R.id.age_input);
        signupButton = (Button) findViewById(R.id.signup_button);
        //photoPath = new String();

        signupButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String username = usernameInput.getText().toString();
                String password = passwordInput.getText().toString();
                String name = nameInput.getText().toString();

                //check if there is any empty field
                if(username.matches("") || password.matches("") || name.matches("")) {
                    Toast.makeText(getApplicationContext(), "There are empty fields!", Toast.LENGTH_SHORT).show();
                    return;
                }

                int age = Integer.parseInt(ageInput.getText().toString());

                //File myFile = new File(photoPath);

                RequestParams params = new RequestParams();

                params.put("username", username);
                params.put("password", password);
                params.put("name", name);
                params.put("age", age);
                /*
                try {
                    params.put("img", myFile);
                } catch (FileNotFoundException e) {
                    Log.d("MyApp", "File not found!!!" + photoPath);
                }
                */
                AsyncHttpClient client = new AsyncHttpClient();

                client.post(MESSAGES_ENDPOINT + "/signup", params, new JsonHttpResponseHandler(){

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        //we save the username in preferences
                        SharedPreferences prefs = getSharedPreferences("Account",Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("username", username);
                        editor.commit();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("username", username);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Registration successful", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        if(statusCode == 401) {
                            Toast.makeText(getApplicationContext(), "User "+username+" already exists!", Toast.LENGTH_LONG).show();
                        }
                        else Toast.makeText(getApplicationContext(), "Something went wrong :(", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }

    /*
    //CODE FOR PICKING UP A PICTURE FROM LOCAL GALLERY

    public void pickPhoto(View view) {
        //TODO: launch the photo picker
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select Picture"), SELECT_PICTURE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK) {
            photoPath = getFilePathFromContentUri(data.getData(), getContentResolver());
            //Toast.makeText(getApplicationContext(), "Path: " + photoPath, Toast.LENGTH_LONG).show();
        }
    }

    private String getFilePathFromContentUri(Uri selectedVideoUri,
                                             ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        Toast.makeText(getApplicationContext(), "Path: " + filePath, Toast.LENGTH_LONG).show();
        return filePath;
    }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
